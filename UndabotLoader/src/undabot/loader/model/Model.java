package undabot.loader.model;

/**
 * Created by Nikola on 19.03.14..
 */
public class Model {
    private String opis;
    private String slikaUrl;
    private String autor;

    public String getSlikaUrl() {
        return slikaUrl;
    }

    public String getAutor() {
        return autor;
    }

    public String getOpis() {
        return opis;
    }



    public void setOpis(String opis) {
        this.opis = opis;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setSlikaUrl(String slikaUrl) {
        this.slikaUrl = slikaUrl;
    }




}
