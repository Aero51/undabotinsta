package undabot.loader.rest;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import undabot.loader.model.Model;

import android.util.Log;

public class InstagramJsonController {
	private ArrayList<Model> lista;
	private String url;
	private JSONArray data = null;
	private Model model;

	public InstagramJsonController(ArrayList<Model> list, String url) {
		this.lista = list;
		this.url = url;

	}

	public ArrayList<Model> getLista() {
		execJsonparse();
		return lista;
	}

	public void setLista(ArrayList<Model> lista) {
		this.lista = lista;
	}

	public void execJsonparse() {

		ResponseHandler sh = new ResponseHandler();
		String jsonStr = sh.makeServiceCall(url, ResponseHandler.GET);

		if (jsonStr != null) {
			try {
				JSONObject jsonObj = new JSONObject(jsonStr);
				data = jsonObj.getJSONArray("data");

				for (int i = 0; i < data.length(); i++) {

					JSONObject c = data.getJSONObject(i);
					String imageUrl = c.getJSONObject("images")
							.getJSONObject("thumbnail").getString("url");

					String caption = null;

					if (!c.isNull("caption")) {
						caption = c.getJSONObject("caption").getString("text");
					} else {
						Log.d("UndabotInsta", "caption Prazan!: " + i);
					}

					String user = c.getJSONObject("user").getString("username");
					model = new Model();
					model.setAutor(user);
					model.setSlikaUrl(imageUrl);

					model.setOpis(caption);
					// Log.d("UndabotInsta", "user: " + user);
					lista.add(model);
				}

			} catch (JSONException e) {
				e.printStackTrace();
				Log.d("UndabotInsta", "JSONException: " + e.toString());
			}
		} else {
			Log.e("UndabotInsta", "Couldn't get any data from the url");
		}

	}

}
