package undabot.loader.loaders;

import java.util.ArrayList;

import android.content.AsyncTaskLoader;
import android.content.Context;
import undabot.loader.model.Model;
import undabot.loader.rest.InstagramJsonController;

public class DataListLoader extends AsyncTaskLoader<ArrayList<Model>> {

	private ArrayList<Model> mlista;
	private InstagramJsonController instaJsonController;
	private String url;

	public DataListLoader(Context context, String url) {
		super(context);
		this.url = url;

	}

	@Override
	public ArrayList<Model> loadInBackground() {
		ArrayList<Model> dlista = new ArrayList<Model>();
		instaJsonController = new InstagramJsonController(dlista, url);
		dlista = instaJsonController.getLista();
		return dlista;
	}

	@Override
	public void deliverResult(ArrayList<Model> data) {
		if (isReset()) {
			if (data != null) {
			}
		}

		ArrayList<Model> oldList = data;
		mlista = oldList;

		if (isStarted()) {
			// If the Loader is currently started, we can immediately
			// deliver its results.
			super.deliverResult(data);
		}

		if (oldList != null) {
			// Log.i("UndabotInsta",
			// "+++ ReleasingReleasing any old data associated with this Loader. +++");
		}
	}

	@Override
	protected void onStartLoading() {
		if (mlista != null) {
			deliverResult(mlista);
		}
		if (takeContentChanged() || mlista == null) {
			forceLoad();
		}
	}

	/**
	 * Handles a request to stop the Loader.
	 */
	@Override
	protected void onStopLoading() {
		// Attempt to cancel the current load task if possible.

		cancelLoad();
	}

	/**
	 * Handles a request to completely reset the Loader.
	 */
	@Override
	protected void onReset() {
		// Ensure the loader is stopped
		onStopLoading();
		// At this point we can release the resources associated with 'apps'
		// if needed.
		if (mlista != null) {
			mlista = null;
		}
	}
}
