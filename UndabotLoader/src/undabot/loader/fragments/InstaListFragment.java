package undabot.loader.fragments;

import java.util.ArrayList;
import java.util.List;



import undabot.loader.R;
import undabot.loader.adapters.AdapterList;
import undabot.loader.loaders.DataListLoader;
import undabot.loader.model.Model;
import undabot.loader.rest.InstagramJsonController;

import android.app.Activity;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

public class InstaListFragment extends ListFragment implements
		LoaderCallbacks<ArrayList<Model>> {

	private static String url = "https://api.instagram.com/v1/media/popular?client_id=e5d3b5e00f70488fa244e02e9ec49e86";
	private static ArrayList<Model> lista = new ArrayList<Model>();;
	private static AdapterList adapter;
	private static LayoutInflater mInflater;
	private static Parcelable listViewState;
	private static MenuItem menuItem;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d("Cycles", "Insta Fragment onCreate ");
		setHasOptionsMenu(true);
		setRetainInstance(true);
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d("Cycles", "Insta Fragment onActivityCreated ");

		mInflater = LayoutInflater.from(getActivity());

		if (adapter == null) {
			adapter = new AdapterList(getActivity(), lista);
		} else {
			setListShown(true);
		}
		getListView().setAdapter(adapter);
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		LoaderManager lm = getLoaderManager();
		lm.initLoader(0, null, this);
		super.onStart();
	}

	@Override
	public void onResume() {
		if (listViewState != null) {
			getListView().onRestoreInstanceState(listViewState);
		}
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		listViewState = getListView().onSaveInstanceState();
	}

	@Override
	public Loader<ArrayList<Model>> onCreateLoader(int id, Bundle args) {
		return new DataListLoader(getActivity(), url);
	}

	@Override
	public void onLoadFinished(Loader<ArrayList<Model>> arg0,
			ArrayList<Model> arg1) {

		if (lista.isEmpty()) // initial delivery
		{
			lista = arg1;
			adapter.setLista(lista);
			adapter.notifyDataSetChanged();
			setListShown(true);

		}
		if (lista.hashCode() != arg1.hashCode()) { // if new list from loader is
													// different from old list
			lista = arg1;
			adapter.setLista(lista);
			adapter.notifyDataSetChanged();
			// setListShown(true);
			menuItem.collapseActionView();
			menuItem.setActionView(null);
			Log.i("Loader", "hashcode liste, hashcode arg1  onLoadFinished: "
					+ lista.hashCode() + "," + arg1.hashCode());
		}
	}

	@Override
	public void onLoaderReset(Loader<ArrayList<Model>> arg0) {
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menumain, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.refresh:
			Toast.makeText(getActivity(), "Refresh was clicked.",
					Toast.LENGTH_SHORT).show();
			menuItem = item;
			menuItem.setActionView(R.layout.progressbar);
			menuItem.expandActionView();
			LoaderManager lm = getLoaderManager();
			lm.restartLoader(0, null, this);
			// setListShown(false);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
