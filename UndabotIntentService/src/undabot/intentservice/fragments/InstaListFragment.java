package undabot.intentservice.fragments;

import java.util.ArrayList;




import undabot.intentservice.R;
import undabot.intentservice.adapters.AdapterList;
import undabot.intentservice.model.ParcelableModel;
import undabot.intentservice.services.SimpleIntentService;
import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class InstaListFragment extends ListFragment {

	private static String url = "https://api.instagram.com/v1/media/popular?client_id=e5d3b5e00f70488fa244e02e9ec49e86";
	private static ArrayList<ParcelableModel> lista = new ArrayList<ParcelableModel>();;
	private static AdapterList adapter;
	private static LayoutInflater mInflater;
	private static Parcelable listViewState;
	private static MenuItem menuItem;
	private static IntentFilter filter;
	private static ResponseReceiver receiver;
	
	
private Intent msgIntent ;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("Cycles", "Insta Fragment onCreate ");
		setHasOptionsMenu(true);
		setRetainInstance(true);

 msgIntent = new Intent(getActivity(), SimpleIntentService.class);
    msgIntent.putExtra(SimpleIntentService.PARAM_IN_MSG, url);
    getActivity().startService(msgIntent);

  filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
    filter.addCategory(Intent.CATEGORY_DEFAULT);
    receiver = new ResponseReceiver();
    getActivity().registerReceiver(receiver, filter);	
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d("Cycles", "Insta Fragment onActivityCreated ");

		mInflater = LayoutInflater.from(getActivity());

		if (adapter == null) {
			adapter = new AdapterList(getActivity(), lista);
		} else {
			setListShown(true);
		}
		getListView().setAdapter(adapter);
		
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		
		super.onStart();
	}

	@Override
	public void onResume() {
		if (listViewState != null) {
			getListView().onRestoreInstanceState(listViewState);
		}
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		listViewState = getListView().onSaveInstanceState();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menumain, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.refresh:
			Toast.makeText(getActivity(), "Refresh was clicked.",
					Toast.LENGTH_SHORT).show();
			menuItem = item;
			menuItem.setActionView(R.layout.progressbar);
			menuItem.expandActionView();
			
		     getActivity().registerReceiver(receiver, filter);
			 getActivity().startService(msgIntent);
			// setListShown(false);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	 public class ResponseReceiver extends BroadcastReceiver {
	        public static final String ACTION_RESP = "undabot.intentservice.LIST_PROCESSED";
	        @Override
	        public void onReceive(Context context, Intent intent) {
	       
	        	 lista =  intent.getParcelableArrayListExtra("model");
	        			adapter.setLista(lista);
	        			adapter.notifyDataSetChanged();
	        			setListShown(true);
	        			if (menuItem!=null)
	        			{
	        			menuItem.collapseActionView();
	        			menuItem.setActionView(null);
	        			}
	        			getActivity().unregisterReceiver(receiver);	
	        }    
	    }
}
