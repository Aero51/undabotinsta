package undabot.intentservice.adapters;

/**
 * Created by Nikola on 19.03.14..
 */
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import undabot.intentservice.R;
import undabot.intentservice.model.ParcelableModel;
import undabot.intentservice.rest.DrawableBackgroundDownloader;



public class AdapterList extends BaseAdapter {
	private final Context context;
	private List<ParcelableModel> lista = new ArrayList<ParcelableModel>();
	public void setLista(List<ParcelableModel> lista) {
		this.lista = lista;
	}

	private DrawableBackgroundDownloader downloader = new DrawableBackgroundDownloader();

	public AdapterList(Context context, ArrayList<ParcelableModel> list) {

		this.context = context;
		lista = list;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolder holder;
		if (view == null) {
			view = LayoutInflater.from(context).inflate(R.layout.list_item,
					parent, false);
			holder = new ViewHolder();
			holder.slika = (ImageView) view.findViewById(R.id.slika);
			holder.autor = (TextView) view.findViewById(R.id.autor);
			holder.opis = (TextView) view.findViewById(R.id.opis);
			view.setTag(holder);

		} else {
			holder = (ViewHolder) view.getTag();
		}

		//Log.i("UndabotInsta", "pozicija:" + position);
		holder.autor.setText(lista.get(position).getAutor());
		holder.opis.setText(lista.get(position).getOpis());

		Drawable drawable = context.getResources().getDrawable(
				R.drawable.no_image);
		downloader.loadDrawable(lista.get(position).getSlikaUrl(), holder.slika,
				drawable);

		return view;
	}

	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public String getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	static class ViewHolder {
		ImageView slika;
		TextView autor;
		TextView opis;
	}
}