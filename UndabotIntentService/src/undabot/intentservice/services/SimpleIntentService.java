package undabot.intentservice.services;



import java.util.ArrayList;


import undabot.intentservice.fragments.InstaListFragment.ResponseReceiver;
import undabot.intentservice.model.ParcelableModel;
import undabot.intentservice.rest.InstagramJsonController;
import android.app.IntentService;
import android.content.Intent;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.util.Log;

public class SimpleIntentService extends IntentService {
    public static final String PARAM_IN_MSG = "imsg";

    public SimpleIntentService() {
        super("SimpleIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String msg = intent.getStringExtra(PARAM_IN_MSG);
        InstagramJsonController instaJsonController;
        ArrayList<ParcelableModel> dlista = new ArrayList<ParcelableModel>();
		instaJsonController = new InstagramJsonController(dlista, msg);
		dlista = instaJsonController.getLista();
        
        
		  Intent broadcastIntent = new Intent();
	        broadcastIntent.setAction(ResponseReceiver.ACTION_RESP);
	        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
	        broadcastIntent.putParcelableArrayListExtra("model", dlista);
	        sendBroadcast(broadcastIntent);  
        stopSelf();
    }
}
