Example Android App for loading instagram popular photos
==================================================

Implemented fetching of json data with Loader, async photo download.
Used Action bar.
##References: 
[Life Before Loaders](http://www.androiddesignpatterns.com/2012/07/loaders-and-loadermanager-background.html)

[How to Use Loaders in Android](http://www.grokkingandroid.com/using-loaders-in-android/)

[The Android Loaders Story](http://learning.icewheel.org/2013/04/the-android-loaders-story.html)

[IntentService Basics](http://code.tutsplus.com/tutorials/android-fundamentals-intentservice-basics--mobile-6183)

[Passing Objects in Intents: Parcelables and More](http://http://thinkandroid.wordpress.com/2012/09/25/passing-objects-in-intents/)

##Projects:

###UndabotLoader

-task implemented using loader

###UndabotIntentService

-basic implementation of intent service with passing of  parseable list to broadcast receiver.Service stops itself upon completion.


##Todo:
 ~~-implement reusing of frags in main activity~~
 
~~-resolve problem with loader reset  upon change orientation of other(flickr) tab.(reset is implemented correctly- problem was in not understanding frag lifecycle - )~~

~~-prevent redownloading of pics on config change~~

~~-resolve json error that shows up occasionally (value null at caption of type org.json.JSON - empty caption produces error )~~

~~-implement saving of position in the list (Insta Frag - first tab)  upon orientation change when on second (flickr) tab~~

~~-implement refresh button in action bar(upper right corner) which downloads new set of popular photos~~

~~-new project with service for background downloading ~~

-implement appcompbat support

-research about memory leaks

##Screenshot:
![Screenshot](http://s29.postimg.org/5in5m87mf/2014_03_27_18_26_00.png)



